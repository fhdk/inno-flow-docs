
## INNOTEC WEB APP
Appens funktion er at danne et sammenspil mellem Innotec og  deres kunde base.

Dette gøres ved at knytte kunden tættere på Innotec gennem håndtering og vedligeholdelse af kundens miljødokumenter samt tilbyde let adgang til genbestilling af kundens produkt portefølje.

## Termer og standarder
Programmeringssprog er udformet på engelsk og for en programmør er meget vanskeligt - rent linguistisk at blande et programmeringssprog med udtryk fra programmørens modersmål. Det bliver i bedste fald noget volapyk i værste fald staller koden fordi der er brugt nationale tegn som æøå eller ÆØÅ.

Det må derfor accepteres at termer og udtryk vil være de engelske udtryk for en given handling, objekt eller bruger rolle - ligesom alle kommentarer i kildekoden vil være på engelsk.

Det eneste som ikke er oversat er akronymerne APB og APV som dækker over

* Arbejdsplads brugsanvisning (APB)
* Kemisk Arbejdsplads vurdering (APV) eller (KAPV)

### Termer
* Sælger - Adviser
* Kunde - Company
* Kontakt - Contact
* Arbejdssted - Workplace
* Dokument - Document
* Salgsordre - SalesOrder / Order
* Ordrelinje - SalesOrderLine / OrderLine
* EOrdre - ESalesOrder
* EOrdreLinje - ESalesOrderLine / ESalesLine

### Innotec roller
* Admin					
* Adviser					
* Supervisor
### Kunde roller
* EDoc
* EShop

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTcwMTU0NDYyNV19
-->